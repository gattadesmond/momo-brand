
function navbarFixed() {
  $(window).scroll(function() {
    var scroll = $(window).scrollTop();
    if (scroll) {
      $(".navbar").addClass("is-fixed");
    } else {
      $(".navbar").removeClass("is-fixed");
    }
  });
  $(window).scroll();
}
navbarFixed();

var userAgent = navigator.userAgent || navigator.vendor || window.opera;
if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
  document.body.classList.add("ios");
}


// var swiper = new Swiper('.swiper-why', {
//   slidesPerView: 'auto',
//   spaceBetween: 0,
// });

var liquidIsMobile = function liquidIsMobile() {
	return (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)
	);
};

if ($("#particles-js").length != 0) {
  window.particlesJS("particles-js", {
    particles: {
      number: {
        value: 35,
        density: {
          enable: !0,
          value_area: 1200
        }
      },
      color: {
        value: ["#8F98E4", "#4BC6B9", "#F5AF6F", "#F78DC5"]
      },
      shape: {
        type: "circle",
        stroke: {
          width: 0,
          color: "#000000"
        },
        polygon: {
          nb_sides: 7
        },
        image: {
          src: "img/github.svg",
          width: 100,
          height: 100
        }
      },
      opacity: {
        value: 1,
        random: !0,
        anim: {
          enable: !0,
          speed: 1,
          opacity_min: 0,
          sync: !1
        }
      },
      size: {
        value: 5,
        random: !0,
        anim: {
          enable: !1,
          speed: 4,
          size_min: 0.3,
          sync: !1
        }
      },
      line_linked: {
        enable: !1,
        distance: 150,
        color: "#ffffff",
        opacity: 0.4,
        width: 1
      },
      move: {
        enable: !0,
        speed: 1,
        direction: "none",
        random: !0,
        straight: !1,
        out_mode: "out",
        bounce: !1,
        attract: {
          enable: !1,
          rotateX: 600,
          rotateY: 600
        }
      }
    },
    interactivity: {
      detect_on: "canvas",
      events: {
        onhover: {
          enable: !0,
          mode: "repulse"
        },
        onclick: {
          enable: !1,
          mode: "repulse"
        },
        resize: !0
      },
      modes: {
        grab: {
          distance: 377.61590372012546,
          line_linked: {
            opacity: 1
          }
        },
        bubble: {
          distance: 250,
          size: 0,
          duration: 2,
          opacity: 0,
          speed: 3
        },
        repulse: {
          distance: 81.20772123013451,
          duration: 0.4
        },
        push: {
          particles_nb: 4
        },
        remove: {
          particles_nb: 2
        }
      }
    },
    retina_detect: !0
  });
}

if ($("#particles-js2").length != 0) {
  window.particlesJS("particles-js2", {
    particles: {
      number: {
        value: 35,
        density: {
          enable: !0,
          value_area: 1200
        }
      },
      color: {
        value: ["#8F98E4", "#4BC6B9", "#F5AF6F", "#F78DC5"]
      },
      shape: {
        type: "circle",
        stroke: {
          width: 0,
          color: "#000000"
        },
        polygon: {
          nb_sides: 7
        },
        image: {
          src: "img/github.svg",
          width: 100,
          height: 100
        }
      },
      opacity: {
        value: 1,
        random: !0,
        anim: {
          enable: !0,
          speed: 1,
          opacity_min: 0,
          sync: !1
        }
      },
      size: {
        value: 5,
        random: !0,
        anim: {
          enable: !1,
          speed: 4,
          size_min: 0.3,
          sync: !1
        }
      },
      line_linked: {
        enable: !1,
        distance: 150,
        color: "#ffffff",
        opacity: 0.4,
        width: 1
      },
      move: {
        enable: !0,
        speed: 1,
        direction: "none",
        random: !0,
        straight: !1,
        out_mode: "out",
        bounce: !1,
        attract: {
          enable: !1,
          rotateX: 600,
          rotateY: 600
        }
      }
    },
    interactivity: {
      detect_on: "canvas",
      events: {
        onhover: {
          enable: !0,
          mode: "repulse"
        },
        onclick: {
          enable: !1,
          mode: "repulse"
        },
        resize: !0
      },
      modes: {
        grab: {
          distance: 377.61590372012546,
          line_linked: {
            opacity: 1
          }
        },
        bubble: {
          distance: 250,
          size: 0,
          duration: 2,
          opacity: 0,
          speed: 3
        },
        repulse: {
          distance: 81.20772123013451,
          duration: 0.4
        },
        push: {
          particles_nb: 4
        },
        remove: {
          particles_nb: 2
        }
      }
    },
    retina_detect: !0
  });
}
jQuery(document).ready(function($) {
  if(liquidIsMobile()){
    var totalWidth = $("#ds-sub-menu").outerWidth()

    $('#ds-sub-menu .people-container').css('width', totalWidth);
  
      var myScrollPos =  $('.is-active').offset().left + $('.is-active').outerWidth(true) / 2 + $('.people-container').scrollLeft() - $('.people-container').width() / 2;
  
  
      $('.people-container').scrollLeft(myScrollPos + 50);
  }

});







  // Caching some stuff..
  const body = document.body;
  const docEl = document.documentElement;


  $(".manual-device").each(function(index, element) {
    var $this = $(this);
    var numSlider = $this.attr("childId");
    var device = null;
    var process = null;

    if (numSlider != undefined) {
      device = $this.find(".manual-device-swiper#hd-sub-swp-" + numSlider);
      process = $this
        .parent()
        .parent()
        .find(".manual-process#hd-sub-ctn-" + numSlider);
    } else {
      numSlider = index;
      device = $this.find(".manual-device-swiper");

      process = $this
        .parent()
        .parent()
        .find(".manual-process ");
    }

    var swiperArray = [];

    swiperArray[numSlider] = new Swiper(device, {
      observer: true,
      observeParents: true,
      on: {
        slideChangeTransitionEnd: function slideChangeTransitionEnd() {
          var num = this.activeIndex;
          process.children(".process_item").removeClass("active");
          process
            .children(".process_item")
            .eq(num)
            .addClass("active");
        }
      }
    });

    process.children(".process_item").each(function(index, element) {
      var num = index;
      $(element).on("click", function(e) {
        e.preventDefault();
        $(this)
          .addClass("active")
          .siblings()
          .removeClass("active");
        swiperArray[numSlider].slideTo(index);
      });
    });
  });

  $(".process__body-content").each(function(index, element) {
    var $this = $(this);
    var height = $this[0].scrollHeight;
    var getHeight = height == "0" ? "auto" : height + "px";
    $this.css("--max-height", getHeight);
  });