("use strict");
var _createClass = (function() {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, descriptor.key, descriptor);
    }
  }
  return function(Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);
    if (staticProps) defineProperties(Constructor, staticProps);
    return Constructor;
  };
})();
function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

(function($, window, document, undefined) {
  "use strict";
  var pluginName = "liquidStack";
  var defaults = {
    sectionSelector: "#content > .vc_row",
    anchors: [],
    easing: "linear",
    css3: true,
    scrollingSpeed: 1000,
    loopTop: false,
    loopBottom: false,
    navigation: false,
    defaultTooltip: "Section",
    prevNextButtons: true,
    animateAnchor: false,
    prevNextLabels: { prev: "Previous", next: "Next" },
    pageNumber: true,
    effect: "none",
    disableOnMobile: true
  };
  var Plugin = (function() {
    function Plugin(element, options) {
      _classCallCheck(this, Plugin);
      this.element = element;
      this.$element = $(element);
      this.options = $.extend({}, defaults, options);
      this._defaults = defaults;
      this._name = pluginName;
      if (
        this.options.disableOnMobile &&
        (liquidIsMobile() || liquidWindowWidth() <= liquidMobileNavBreakpoint())
      )
        return false;
      this.anchors = [];
      this.tooltips = [];
      this.sectionsLuminance = [];
      this.$body = $("body");
      this.$sections = $(this.options.sectionSelector);
      this.$mainHeader = $(".main-header");
      this.$mainFooter = $(".main-footer");
      this.$ppNav = null;
      this.$prevNextButtons = $(".lqd-stack-prevnext-wrap");
      this.$pageNumber = $(".lqd-stack-page-number");
      this.$backToTopButton = $("[data-back-to-top]");
      this.timeoutId;
      this.animationIsFinished = false;
      this.build();
      this.addClassnames();
      this.eachSection();
      this.init();
    }
    _createClass(Plugin, [
      {
        key: "build",
        value: function build() {
          if (this.$mainFooter.length) {
            this.$sections.push(this.$mainFooter.get(0));
            this.$element.append(this.$mainFooter);
          }
          this.$element.children("style").appendTo("head");
          this.$element.children(".vc_row-full-width").remove();
        }
      },
      {
        key: "addClassnames",
        value: function addClassnames() {
          var options = this.options;
          this.$mainFooter.length &&
            this.$mainFooter.addClass("vc_row pp-section pp-auto-height");
          options.navigation && this.$body.addClass("lqd-stack-has-nav");
          options.prevNextButtons &&
            this.$body.addClass("lqd-stack-has-prevnext-buttons");
          options.pageNumber &&
            this.$body.addClass("lqd-stack-has-page-numbers");
          options.effect !== "none" &&
            this.$body.addClass("lqd-stack-effect-enabled");
          this.$body.addClass("lqd-stack-effect-" + options.effect);
          this.$body.add("html").addClass("overflow-hidden");
          $("html").addClass("pp-enabled");
        }
      },
      {
        key: "eachSection",
        value: function eachSection() {
          var _this = this;
          $.each(this.$sections, function(i, section) {
            _this.wrapInnerContent(section);
            _this.makeScrollable(section);
            _this.setSectionsLuminance(section);
            _this.setAnchors(i, section);
            _this.setTooltips(i, section);
          });
        }
      },
      {
        key: "wrapInnerContent",
        value: function wrapInnerContent(section) {
          $(section).wrapInner('<div class="lqd-stack-section-inner" />');
        }
      },
      {
        key: "makeScrollable",
        value: function makeScrollable(section) {
          var $section = $(section);
          var $sectionContainer = $section.children(".lqd-stack-section-inner");
          if ($sectionContainer.height() > $section.height()) {
            $section.addClass("pp-scrollable");
          }
        }
      },
      {
        key: "setAnchors",
        value: function setAnchors(i, section) {
          if (section.hasAttribute("id")) {
            this.anchors[i] = section.getAttribute("id");
          } else if (section.hasAttribute("data-tooltip")) {
            this.anchors[i] = section
              .getAttribute("data-tooltip")
              .replace(new RegExp(" ", "g"), "-")
              .toLowerCase();
          } else {
            if (!section.hasAttribute("data-anchor")) {
              this.anchors[i] = this.options.defaultTooltip + "-" + (i + 1);
            } else {
              this.anchors[i] = section.getAttribute("data-anchor");
            }
          }
        }
      },
      {
        key: "setTooltips",
        value: function setTooltips(i, section) {
          if (!section.hasAttribute("data-tooltip")) {
            this.tooltips[i] = this.options.defaultTooltip + " " + (i + 1);
          } else {
            this.tooltips[i] = section.getAttribute("data-tooltip");
          }
        }
      },
      {
        key: "setSectionsLuminance",
        value: function setSectionsLuminance(section) {
          var $section = $(section);
          var contentBgColor = this.$element.css("backgroundColor");
          var sectionBgColor =
            $section.css("backgroundColor") || contentBgColor || "#fff";
          if (section.hasAttribute("data-section-luminance")) {
            this.sectionsLuminance.push(
              $section.attr("data-section-luminance")
            );
          } else {
            this.sectionsLuminance.push(
              tinycolor(sectionBgColor).getLuminance() <= 0.5 ? "dark" : "light"
            );
          }
        }
      },
      {
        key: "init",
        value: function init() {
          var _options = this.options,
            sectionSelector = _options.sectionSelector,
            anchors = _options.anchors,
            easing = _options.easing,
            css3 = _options.css3,
            scrollingSpeed = _options.scrollingSpeed,
            loopTop = _options.loopTop,
            loopBottom = _options.loopBottom,
            navigation = _options.navigation,
            animateAnchor = _options.animateAnchor;
          if (navigation && this.tooltips.length > 0) {
            navigation = {};
            navigation.tooltips = this.tooltips;
          }
          if (anchors) {
            anchors = this.anchors;
          }
          this.$element.pagepiling({
            sectionSelector: sectionSelector,
            anchors: anchors,
            easing: easing,
            css3: css3,
            scrollingSpeed: scrollingSpeed,
            loopTop: loopTop,
            loopBottom: loopBottom,
            animateAnchor: animateAnchor,
            navigation: navigation,
            afterRender: this.afterRender.bind(this),
            onLeave: this.onLeave.bind(this),
            afterLoad: this.afterLoad.bind(this)
          });
        }
      },
      {
        key: "appendPrevNextButtons",
        value: function appendPrevNextButtons() {
          var prevNextLabels = this.options.prevNextLabels;
          this.$prevNextButtons = $('<div class="lqd-stack-prevnext-wrap" />');
          var $prevButton = $(
            '<button class="lqd-stack-prevnext-button lqd-stack-prev-button">\n\t\t\t\t<span class="lqd-stack-button-labbel">' +
              prevNextLabels.prev +
              '</span>\n\t\t\t\t<span class="lqd-stack-button-ext">\n\t\t\t\t\t<svg width="36px" height="36px" class="lqd-stack-button-circ" viewBox="0 0 36 36" version="1.1" xmlns="http://www.w3.org/2000/svg" fill="none" stroke="#000">\n\t\t\t\t\t\t<path d="M17.89548,35.29096 C27.5027383,35.29096 35.29096,27.5027383 35.29096,17.89548 C35.29096,8.28822168 27.5027383,0.5 17.89548,0.5 C8.28822168,0.5 0.5,8.28822168 0.5,17.89548 C0.5,27.5027383 8.28822168,35.29096 17.89548,35.29096 Z"></path>\n\t\t\t\t\t</svg>\n\t\t\t\t\t<svg width="36px" height="36px" class="lqd-stack-button-circ lqd-stack-button-circ-clone" viewBox="0 0 36 36" version="1.1" xmlns="http://www.w3.org/2000/svg" fill="none" stroke="#000">\n\t\t\t\t\t\t<path d="M17.89548,35.29096 C27.5027383,35.29096 35.29096,27.5027383 35.29096,17.89548 C35.29096,8.28822168 27.5027383,0.5 17.89548,0.5 C8.28822168,0.5 0.5,8.28822168 0.5,17.89548 C0.5,27.5027383 8.28822168,35.29096 17.89548,35.29096 Z"></path>\n\t\t\t\t\t</svg>\n\t\t\t\t\t<svg xmlns="http://www.w3.org/2000/svg" class="lqd-stack-button-arrow" width="12.5px" height="13.5px" viewbox="0 0 12.5 13.5" fill="none" stroke="#000">\n\t\t\t\t\t\t<path d="M11.489,6.498 L0.514,12.501 L0.514,0.495 L11.489,6.498 Z"/>\n\t\t\t\t\t</svg>\n\t\t\t\t</span>\n\t\t\t</button>'
          );
          var $nextButton = $(
            '<button class="lqd-stack-prevnext-button lqd-stack-next-button">\n\t\t\t\t<span class="lqd-stack-button-labbel">' +
              prevNextLabels.next +
              '</span>\n\t\t\t\t<span class="lqd-stack-button-ext">\n\t\t\t\t\t<svg width="36px" height="36px" class="lqd-stack-button-circ" viewBox="0 0 36 36" version="1.1" xmlns="http://www.w3.org/2000/svg" fill="none" stroke="#000">\n\t\t\t\t\t\t<path d="M17.89548,35.29096 C27.5027383,35.29096 35.29096,27.5027383 35.29096,17.89548 C35.29096,8.28822168 27.5027383,0.5 17.89548,0.5 C8.28822168,0.5 0.5,8.28822168 0.5,17.89548 C0.5,27.5027383 8.28822168,35.29096 17.89548,35.29096 Z"></path>\n\t\t\t\t\t</svg>\n\t\t\t\t\t<svg width="36px" height="36px" class="lqd-stack-button-circ lqd-stack-button-circ-clone" viewBox="0 0 36 36" version="1.1" xmlns="http://www.w3.org/2000/svg" fill="none" stroke="#000">\n\t\t\t\t\t\t<path d="M17.89548,35.29096 C27.5027383,35.29096 35.29096,27.5027383 35.29096,17.89548 C35.29096,8.28822168 27.5027383,0.5 17.89548,0.5 C8.28822168,0.5 0.5,8.28822168 0.5,17.89548 C0.5,27.5027383 8.28822168,35.29096 17.89548,35.29096 Z"></path>\n\t\t\t\t\t</svg>\n\t\t\t\t\t<svg xmlns="http://www.w3.org/2000/svg" class="lqd-stack-button-arrow" width="12.5px" height="13.5px" viewbox="0 0 12.5 13.5" fill="none" stroke="#000">\n\t\t\t\t\t\t<path d="M11.489,6.498 L0.514,12.501 L0.514,0.495 L11.489,6.498 Z"/>\n\t\t\t\t\t</svg>\n\t\t\t\t</span>\n\t\t\t</button>'
          );
          this.$prevNextButtons.append($prevButton.add($nextButton));
          !this.$body.children(".lqd-stack-prevnext-wrap").length &&
            this.$body.append(this.$prevNextButtons);
        }
      },
      {
        key: "prevNextButtonsEvents",
        value: function prevNextButtonsEvents() {
          var $prevButton = this.$prevNextButtons.find(
            ".lqd-stack-prev-button"
          );
          var $nextButton = this.$prevNextButtons.find(
            ".lqd-stack-next-button"
          );
          $prevButton.on("click", function() {
            $.fn.pagepiling.moveSectionUp();
          });
          $nextButton.on("click", function() {
            $.fn.pagepiling.moveSectionDown();
          });
        }
      },
      {
        key: "appendPageNumber",
        value: function appendPageNumber() {
          var totalSections = this.$sections.not(".main-footer").length;
          this.$pageNumber = $('<div class="lqd-stack-page-number" />');
          var $pageNumnerCounter = $(
            '<span class="lqd-stack-page-number-counter">\n\t\t\t\t<span class="lqd-stack-page-number-current"></span>\n\t\t\t\t<span class="lqd-stack-page-number-passed"></span>\n\t\t\t</span>'
          );
          var $pageNumnerTotal = $(
            '<span class="lqd-stack-page-number-total">' +
              (totalSections < 10 ? "0" : "") +
              totalSections +
              "</span>"
          );
          this.$pageNumber.append($pageNumnerCounter);
          this.$pageNumber.append($pageNumnerTotal);
          !this.$body.children(".lqd-stack-page-number").length &&
            this.$body.append(this.$pageNumber);
        }
      },
      {
        key: "setPageNumber",
        value: function setPageNumber(index) {
          var $currentPageNumber = this.$pageNumber.find(
            ".lqd-stack-page-number-current"
          );
          var $passedPageNumber = this.$pageNumber.find(
            ".lqd-stack-page-number-passed"
          );
          $passedPageNumber.html($currentPageNumber.html());
          $currentPageNumber.html("" + (index < 10 ? "0" : "") + index);
        }
      },
      {
        key: "addDirectionClassname",
        value: function addDirectionClassname(direction) {
          if (direction == "down") {
            this.$body
              .removeClass("lqd-stack-moving-up")
              .addClass("lqd-stack-moving-down");
          } else if (direction == "up") {
            this.$body
              .removeClass("lqd-stack-moving-down")
              .addClass("lqd-stack-moving-up");
          }
        }
      },
      {
        key: "addLuminanceClassnames",
        value: function addLuminanceClassnames(index) {
          this.$body
            .removeClass("lqd-stack-active-row-dark lqd-stack-active-row-light")
            .addClass("lqd-stack-active-row-" + this.sectionsLuminance[index]);
        }
      },
      {
        key: "initShortcodes",
        value: function initShortcodes($destinationRow) {
          $(
            "[data-custom-animations]",
            $destinationRow
          ).liquidCustomAnimations();
          $destinationRow.is("[data-custom-animations]") &&
            $destinationRow.liquidCustomAnimations();
          $("[data-dynamic-shape]", $destinationRow).liquidDynamicShape();
          $("[data-reveal]", $destinationRow).liquidReveal();
          $("[data-particles=true]", $destinationRow).liquidParticles();
        }
      },
      {
        key: "initBackToTop",
        value: function initBackToTop(rowIndex) {
          if (rowIndex > 1) {
            this.$backToTopButton.addClass("is-visible");
          } else {
            this.$backToTopButton.removeClass("is-visible");
          }
          $("a", this.$backToTopButton).on("click", function(event) {
            event.preventDefault();
            $.fn.pagepiling.moveTo(1);
          });
        }
      },
      {
        key: "afterRender",
        value: function afterRender() {
          this.$body.addClass("lqd-stack-initiated");
          this.$ppNav = $("#pp-nav");
          if (this.$mainFooter.length) {
            this.$ppNav
              .find("li")
              .last()
              .addClass("hide");
            this.$body.addClass("lqd-stack-has-footer");
          }
          this.initShortcodes(this.$sections.first());
          this.addLuminanceClassnames(0);
          this.options.prevNextButtons && this.appendPrevNextButtons();
          this.options.prevNextButtons && this.prevNextButtonsEvents();
          this.options.pageNumber && this.appendPageNumber();
          this.options.pageNumber && this.setPageNumber(1);
        }
      },
      {
        key: "onLeave",
        value: function onLeave(index, nextIndex, direction) {
          var $destinationRow = $(this.$sections[nextIndex - 1]);
          var $originRow = $(this.$sections[index - 1]);
          if (
            !$destinationRow.is(".main-footer") &&
            !$originRow.is(".main-footer")
          ) {
            this.$body.addClass("lqd-stack-moving");
            this.setPageNumber(nextIndex);
            $destinationRow
              .removeClass("lqd-stack-row-leaving")
              .addClass("lqd-stack-row-entering");
            $originRow
              .removeClass("lqd-stack-row-entering")
              .addClass("lqd-stack-row-leaving");
            this.addLuminanceClassnames(nextIndex - 1);
          } else if ($originRow.is(".main-footer")) {
            $originRow.addClass("lqd-stack-row-leaving");
          }
          if ($destinationRow.is(".main-footer")) {
            this.$body.addClass("lqd-stack-footer-active");
            $originRow.css("transform", "none");
          } else {
            this.$body.removeClass("lqd-stack-footer-active");
          }
          this.addDirectionClassname(direction);
          this.initShortcodes($destinationRow);
          this.$backToTopButton.length && this.initBackToTop(nextIndex);
        }
      },
      {
        key: "afterLoad",
        value: function afterLoad(anchorLink, index) {
          $(this.$sections).removeClass(
            "will-change lqd-stack-row-entering lqd-stack-row-leaving"
          );
          this.$body.removeClass(
            "lqd-stack-moving lqd-stack-moving-up lqd-stack-moving-down"
          );
        }
      }
    ]);
    return Plugin;
  })();
  $.fn[pluginName] = function(options) {
    return this.each(function() {
      var pluginOptions = $(this).data("stack-options") || options;
      if (!$.data(this, "plugin_" + pluginName)) {
        $.data(this, "plugin_" + pluginName, new Plugin(this, pluginOptions));
      }
    });
  };
})(jQuery, window, document);


jQuery(document).ready(function($) {
  if ($("body").hasClass("compose-mode")) return false;
  $("[data-liquid-stack=true]").liquidStack();
});
